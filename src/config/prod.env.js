module.exports = {
    NODE_ENV: '"production"',
    port: 80,
    secret: '1B2781C41E5AAE48AFC26438677DA',
    session_secret: 'F3F4EDADEBA2AA1279212822E29AD',
    axios: {
        userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
    }
};