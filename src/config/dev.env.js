const merge = require('webpack-merge');
const prodEnv = require('./prod.env');

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    port: 7000,
    auth_ms_url: 'https://greencube-users-ms.herokuapp.com',
    auth_ms_port: 80,
    plant_ms_url: 'https://greencube-plants-ms.herokuapp.com',
    plant_ms_port: 80,
    hardware_url: 'http://greencube-hardware.ddns.net',
    hardware_port: 80,
    secret: 'secret',
    session_secret: 'session_secret',
});