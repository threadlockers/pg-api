const express = require('express');
const mysql = require('mysql');
// const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');

const config = require('./config');

Promise = require('bluebird');

/* const con = mysql.createConnection({
    host: "db4free.net",
    port: "3306",
    database: "pgusersms",
    user: "pgusersms",
    password: "12345678"
  });
  
  con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  }); */

//Setting up express + bodyParser and other middleware
const api = express();

// api.use(morgan('combined'));
api.use(bodyParser.json());
api.use(bodyParser.text());
api.use(cors());

//Defining routes
const userMs = require('./routes/user.ms');
const plantMs = require('./routes/plant.ms');
const hardware = require('./routes/hardware.ms');

api.use('/user-ms', userMs);
api.use('/plant-ms', plantMs);
api.use('/hardware', hardware);

const port = process.env.PORT || config.env.port;

api.listen(port, '0.0.0.0', () => {
    console.log(`API Running on port ${port}`);
});