const jwt = require('jsonwebtoken');
const config = require('../config');

module.exports = function (req, res, next) {
    let token = req.body;
    jwt.sign(token, config.env.secret, {noTimestamp: true}, (err, encoded) => {
        if(err) return res.status(401).send({message: 'The request could not be validated!'});
        req.body = encoded;
        next();
    });
}