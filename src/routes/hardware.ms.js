const express = require('express');
const router = express.Router();
const jwt_controller = require('../controllers/jwt.controller');
const config = require('../config');
const axios = require('axios');

router.post('/get-stats', jwt_controller, (req, res) => {
    axios.post(`${config.env.hardware_url}/stats/get`,
    req.body,
    {
        headers: {
            'Content-Type': 'text/plain',
            'User-Agent': `${config.env.axios.userAgent}`,
            'Accept': '*/*',
        }
    })
    .then(function (response) {
        return res.status(200).send(response.data);
    })
    .catch(function (error) {
        console.log(error);
        return res.status(500).send(error.response.data);
    });
});

router.post('/watering-start', jwt_controller, (req, res) => {
    axios.post(`${config.env.hardware_url}/watering/watering-start`,
    req.body,
    {
        headers: {
            'Content-Type': 'text/plain',
            'User-Agent': `${config.env.axios.userAgent}`,
            'Accept': '*/*',
        }
    })
    .then(function (response) {
        return res.status(200).send(response.data);
    })
    .catch(function (error) {
        console.log(error);
        return res.status(500).send(error.response.data);
    });
});

module.exports = router;