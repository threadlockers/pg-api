const express = require('express');
const router = express.Router();
const jwt_controller = require('../controllers/jwt.controller');
const config = require('../config');
const axios = require('axios');

router.get('/available', jwt_controller, (req, res) => {
    axios.post(`${config.env.plant_ms_url}/api/available`,
    req.body,
    {
        headers: {
            'Content-Type': 'text/plain',
            'User-Agent': `${config.env.axios.userAgent}`,
            'Accept': '*/*',
        }
    })
    .then(function (response) {
        return res.status(200).send(response.data);
    })
    .catch(function (error) {
        console.log(error);
        return res.status(500).send(error.response.data);
    });
});

router.post('/transfer', jwt_controller, (req, res) => {
    axios.post(`${config.env.plant_ms_url}/api/transfer`,
    req.body,
    {
        headers: {
            'Content-Type': 'text/plain',
            'User-Agent': `${config.env.axios.userAgent}`,
            'Accept': '*/*',
        }
    })
    .then(function (response) {
        return res.status(200).send(response.data);
    })
    .catch(function (error) {
        console.log(error);
        return res.status(500).send(error.response.data);
    });
});

router.post('/owned', jwt_controller, (req, res) => {
    axios.post(`${config.env.plant_ms_url}/api/owned`,
    req.body,
    {
        headers: {
            'Content-Type': 'text/plain',
            'User-Agent': `${config.env.axios.userAgent}`,
            'Accept': '*/*',
        }
    })
    .then(function (response) {
        return res.status(200).send(response.data);
    })
    .catch(function (error) {
        console.log(error);
        return res.status(500).send(error.response.data);
    });
});

router.post('/plant-data', jwt_controller, (req, res) => {
    axios.post(`${config.env.plant_ms_url}/api/plant-data`,
    req.body,
    {
        headers: {
            'Content-Type': 'text/plain',
            'User-Agent': `${config.env.axios.userAgent}`,
            'Accept': '*/*',
        }
    })
    .then(function (response) {
        return res.status(200).send(response.data);
    })
    .catch(function (error) {
        console.log(error);
        return res.status(500).send(error.response.data);
    });
});

module.exports = router;