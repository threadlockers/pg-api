const express = require('express');
const router = express.Router();
const jwt_controller = require('../controllers/jwt.controller');
const config = require('../config');
const axios = require('axios');


router.post('/new-user', jwt_controller, (req, res) => {
    axios.post(`${config.env.auth_ms_url}/api/new-user`,
        req.body,
        {
            headers: {
                'Content-Type': 'text/plain',
                'User-Agent': `${config.env.axios.userAgent}`,
                'Accept': '*/*',
            }
        })
        .then(function (response) {
            return res.status(200).send(response.data);
        })
        .catch(function (error) {
            console.log(error);
            return res.status(400).send(error.response.data);
        });
});

router.post('/authenticate-user', jwt_controller, (req, res) => {
    axios.post(`${config.env.auth_ms_url}/api/authenticate-user`,
        req.body,
        {
            headers: {
                'Content-Type': 'text/plain',
                'User-Agent': `${config.env.axios.userAgent}`,
                'Accept': '*/*',
            }
        })
        .then(function (response) {
            return res.status(200).send(response.data);
        })
        .catch(function (error) {
            console.log(error);
            return res.status(401).send({message: "Wrong user/password combination!"});
        });
});

router.post('/add-credits', jwt_controller, (req, res) => {
    axios.post(`${config.env.auth_ms_url}/api/add-credits`,
        req.body,
        {
            headers: {
                'Content-Type': 'text/plain',
                'User-Agent': `${config.env.axios.userAgent}`,
                'Accept': '*/*',
            }
        })
        .then(function (response) {
            return res.status(200).send(response.data);
        })
        .catch(function (error) {
            console.log(error);
            return res.status(401).send(error.response.data);
        });
});

router.post('/user-data', jwt_controller, (req, res) => {
    axios.post(`${config.env.auth_ms_url}/api/user-data`,
        req.body,
        {
            headers: {
                'Content-Type': 'text/plain',
                'User-Agent': `${config.env.axios.userAgent}`,
                'Accept': '*/*',
                'Authorization': req.get('Authorization'),
            }
        })
        .then(function (response) {
            return res.status(200).send(response.data);
        })
        .catch(function (error) {
            console.log(error);
            return res.status(401).send(error.response.data);
        });
})

module.exports = router;